package com.devcamp.jbr470.jbr470;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnimalController {
    @CrossOrigin
    @GetMapping("/cats")
    public ArrayList<Cat> getListCat() {
        ArrayList<Cat> listCat = new ArrayList<>();

        Cat cat1 = new Cat("Mi");
        Cat cat2 = new Cat("Meo");
        Cat cat3 = new Cat("Lợn");

        listCat.add(cat1);
        listCat.add(cat2);
        listCat.add(cat3);

        return listCat;
    }

    @CrossOrigin
    @GetMapping("/dogs")
    public ArrayList<Dog> getListDog() {
        ArrayList<Dog> listDog = new ArrayList<>();

        Dog dog1 = new Dog("Milu");
        Dog dog2 = new Dog("Bo");
        Dog dog3 = new Dog("Bu");

        listDog.add(dog1);
        listDog.add(dog2);
        listDog.add(dog3);

        return listDog;
    }
}
