package com.devcamp.jbr470.jbr470;

public class Mammal extends Animal {

    public Mammal(String name) {
        super(name);
        //TODO Auto-generated constructor stub
    }
    
    @Override
    public String toString() {
        return "Mammal[Animal[name=" + this.getName() + "]]";
    }
}
