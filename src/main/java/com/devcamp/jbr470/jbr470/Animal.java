package com.devcamp.jbr470.jbr470;

public class Animal {
    private String name;

    public Animal(String name) {
        this.setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Animal[name=" + this.getName() + "]";
    }
}
